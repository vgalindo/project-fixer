import { UiReducer, initialState } from './ui.reducer';

describe('Ui Reducer', () => {
  describe('unknown action', () => {
    it('should return the initial state', () => {
      const action = {} as any;

      const result = UiReducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });
});
