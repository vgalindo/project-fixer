import { Action } from '@ngrx/store';
import {AppError} from './model/app-error';
import * as fromUI from './ui.actions';

export interface UiState {
  isLoading: boolean;
  error: AppError;
}

export const initialState: UiState = {
  isLoading: false,
  error: null,
};

export function UiReducer(state = initialState, action: fromUI.UiActions): UiState {
  switch (action.type) {
    case fromUI.UiActionTypes.ToogleLoading:
      return Object.assign({}, {
        ...state,
        isLoading: !state.isLoading,
      });

    default:
      return state;
  }
}

export const getIsLoading = (state: UiState) => state.isLoading;
export const getError = (state: UiState) => state.error;
