import { Injectable } from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import * as fromExchange from '../../reducers/exchange-state/exchange.actions';
import * as Exchange from '../../reducers/exchange-state/exchange.reducer';
import * as fromUi from '../../reducers/ui-state/ui.actions';
import * as Ui from '../../reducers/ui-state/ui.reducer';
import {Observable} from 'rxjs';
import {Action} from '@ngrx/store';
import {map} from 'rxjs/operators';

@Injectable()
export class UiEffects {

  constructor(private actions$: Actions) {}

  @Effect()
  public showLoader$: Observable<Action> = this.actions$.pipe(
    ofType(fromExchange.ExchangeActionTypes.FetchExchangeRate),
    map(() => new fromUi.ToogleLoading()),
  );
  @Effect()
  public hiddeLoader$: Observable<Action> = this.actions$.pipe(
    ofType(fromExchange.ExchangeActionTypes.SetExchangeRate),
    map(() => new fromUi.ToogleLoading()),
  );
}
