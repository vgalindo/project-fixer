export interface AppError {
  code: string;
  detail: string;
}
