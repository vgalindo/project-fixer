import { Action } from '@ngrx/store';
import {AppError} from './model/app-error';

export enum UiActionTypes {
  ToogleLoading = '[Ui] ToogleLoading',
  SetError = '[Ui] SetError',
  ClearError = '[Ui] ClearError',
}

export class ToogleLoading implements Action {
  readonly type = UiActionTypes.ToogleLoading;
}
export class SetError implements Action {
  readonly type = UiActionTypes.SetError;
  constructor(
    public payload: AppError,
  ) {}
}
export class ClearError implements Action {
  readonly type = UiActionTypes.ClearError;
}

export type UiActions =
  ToogleLoading |
  SetError |
  ClearError;
