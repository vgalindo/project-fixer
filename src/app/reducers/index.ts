import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import { environment } from '../../environments/environment';
import * as Ui from './ui-state/ui.reducer';
import * as Exchange from './exchange-state/exchange.reducer';
import {SelectCurrency} from './exchange-state/exchange.actions';

export interface RootState {
  ui: Ui.UiState;
  exchange: Exchange.ExchangeState;
}

export const RootReducers: ActionReducerMap<RootState> = {
  ui: Ui.UiReducer,
  exchange: Exchange.ExchangeReducer
};


export const metaReducers: MetaReducer<RootState>[] = !environment.production ? [] : [];

export const getUiState = createFeatureSelector<Ui.UiState>('ui');

export const getUiStateIsLoading = createSelector(getUiState, Ui.getIsLoading);
export const getUiStateError = createSelector(getUiState, Ui.getError);

export const getExchangeState = createFeatureSelector<Exchange.ExchangeState>('exchange');

export const getExchangeStateRate = createSelector(getExchangeState, Exchange.getRate);
export const getExchangeStateExchange = createSelector(getExchangeState, Exchange.getLastExchange);
export const getExchangeStateSelectCurrency = createSelector(getExchangeState, Exchange.getSelectedCurrency);
