import { Action } from '@ngrx/store';
import {Currency, ExchangeRate} from './model/exchange-rate';

export enum ExchangeActionTypes {
  FetchExchangeRate = '[Exchange] FetchExchangeRate',
  SetExchangeRate = '[Exchange] SetExchangeRate',
  SelectCurrency = '[Exchange] SelectCurrency',
  CalculateExchange = '[Exchange] CalculateExchange',
}

export class FetchExchangeRate implements Action {
  readonly type = ExchangeActionTypes.FetchExchangeRate;

  constructor(
    public payload: Currency,
  ) {}
}
export class SetExchangeRate implements Action {
  readonly type = ExchangeActionTypes.SetExchangeRate;

  constructor(
    public payload: ExchangeRate
  ) {}
}
export class CalculateExchange implements Action {
  readonly type = ExchangeActionTypes.CalculateExchange;

  constructor(
    public payload: number,
  ) {}
}
export class SelectCurrency implements Action {
  readonly type = ExchangeActionTypes.SelectCurrency;

  constructor(
    public payload: Currency,
  ) {}
}

export type ExchangeActions =
  FetchExchangeRate |
  SetExchangeRate |
  CalculateExchange |
  SelectCurrency;
