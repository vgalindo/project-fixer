import { TestBed, inject } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs/Observable';

import { ExchangeEffects } from './exchange.effects';
import {CurrencyExchangeService} from '../../currency-exchange/services/currency-exchange.service';
import {Store, StoreModule} from '@ngrx/store';
import * as RootState from '../index';

class MockCurrencyExchangeService{}

describe('ExchangeService', () => {
  let actions$: Observable<any>;
  let effects: ExchangeEffects;
  let store: Store<RootState.RootState>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ExchangeEffects,
        { provide: CurrencyExchangeService,
          useClass: MockCurrencyExchangeService},
        provideMockActions(() => actions$)
      ],
      imports: [
        StoreModule.forRoot(RootState.RootReducers),
      ]
    });

    effects = TestBed.get(ExchangeEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
