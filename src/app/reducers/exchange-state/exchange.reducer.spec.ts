import { ExchangeReducer, initialState } from './exchange.reducer';

describe('Exchange Reducer', () => {
  describe('unknown action', () => {
    it('should return the initial state', () => {
      const action = {} as any;

      const result = ExchangeReducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });
});
