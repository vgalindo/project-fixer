export interface ExchangeRate {
  'success': boolean;
  'timestamp': number;
  'base': string;
  'date': string;
  'rates': Rate;
};

export interface Rate {
  USD?: number;
  AUD?: number;
  CAD?: number;
  PLN?: number;
  MXN?: number;
};

export interface Currency {
  value: string;
  code: string
};
