import { Action } from '@ngrx/store';
import {Currency, ExchangeRate} from './model/exchange-rate';
import * as fromExchange from './exchange.actions';
import {SelectCurrency} from './exchange.actions';


export interface ExchangeState {
  rate: ExchangeRate;
  selectedCurrency: Currency;
  lastExchange: number;
}

export const initialState: ExchangeState = {
  rate: null,
  selectedCurrency: {value: '$', code: 'USD'},
  lastExchange: 0,
};

export function ExchangeReducer(state = initialState, action: fromExchange.ExchangeActions): ExchangeState {
  switch (action.type) {
    case fromExchange.ExchangeActionTypes.SetExchangeRate:
      return Object.assign({}, {
        ...state,
        rate: action.payload,
      });
    case fromExchange.ExchangeActionTypes.SelectCurrency:
      return Object.assign({}, {
        ...state,
        selectedCurrency: action.payload,
      });
    case fromExchange.ExchangeActionTypes.CalculateExchange:
      console.log('action.payload', action.payload);
      console.log('state.rate.rates.USD', state.rate);
      const value = action.payload / state.rate.rates.USD;
      return Object.assign({}, {
        ...state,
        lastExchange: value,
      });

    default:
      return state;
  }
}

export const getRate = (state: ExchangeState) => state.rate;
export const getLastExchange = (state: ExchangeState) => state.lastExchange;
export const getSelectedCurrency = (state: ExchangeState) => state.selectedCurrency;
