import { Injectable } from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {CurrencyExchangeService} from '../../currency-exchange/services/currency-exchange.service';
import {Observable} from 'rxjs';
import {Action, Store} from '@ngrx/store';
import * as fromExchange from '../../reducers/exchange-state/exchange.actions';
import * as Exchange from '../../reducers/exchange-state/exchange.reducer';
import * as fromUi from '../../reducers/ui-state/ui.actions';
import * as Ui from '../../reducers/ui-state/ui.reducer';
import * as RootState from '../index';
import {catchError, map, single, switchMap, take} from 'rxjs/operators';
import {of} from 'rxjs/observable/of';
import {CalculateExchange} from './exchange.actions';
import {ExchangeRate} from './model/exchange-rate';
import { DateTime } from 'luxon';

@Injectable()
export class ExchangeEffects {

  constructor(
    private actions$: Actions,
    private exchangeService: CurrencyExchangeService,
    private store: Store<RootState.RootState>,
  ) {}

  @Effect()
  public fetchExchangeRate$: Observable<Action> = this.actions$.pipe(
    ofType(fromExchange.ExchangeActionTypes.FetchExchangeRate),
    switchMap(action => this.exchangeService.fetchRates(action).pipe(
      map(response => new fromExchange.SetExchangeRate(response)),
      catchError(error => of(new fromUi.SetError({code: '001', detail: error})))
    ))
  )

  @Effect({ dispatch: false })
  public calculateExchange$ = this.actions$.pipe(
    ofType(fromExchange.ExchangeActionTypes.CalculateExchange),
    switchMap(() => this.store.select(RootState.getExchangeState).pipe(
      take(1),
      map((rateState: Exchange.ExchangeState) => {
        const now = DateTime.utc();
        const lastreq = DateTime.fromMillis(rateState.rate.timestamp * 1000);
        const diff = now.diff(lastreq, 'minutes').toObject();
        if (diff.minutes >= 10) {
          this.store.dispatch(new fromExchange.FetchExchangeRate(rateState.selectedCurrency));
        }
      })
    )));
}
