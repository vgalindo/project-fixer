import { TestBed, inject } from '@angular/core/testing';

import { CurrencyExchangeService } from './currency-exchange.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {CurrencyPipe} from '@angular/common';
import * as RootState from '../../reducers';
import {Store, StoreModule} from '@ngrx/store';

describe('CurrencyExchangeService', () => {
  let store: Store<RootState.RootState>;

  beforeEach(() => {
    TestBed.configureTestingModule({

      providers: [
        CurrencyExchangeService,
      ],
      imports: [
        HttpClientTestingModule,
        StoreModule.forRoot(RootState.RootReducers),
      ]
    });
  });

  it('should be created', inject([CurrencyExchangeService], (service: CurrencyExchangeService) => {
    expect(service).toBeTruthy();
  }));
});
