import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import * as Root from '../../reducers/index';
import {Store} from '@ngrx/store';
import {ExchangeRate} from '../../reducers/exchange-state/model/exchange-rate';
import {environment} from '../../../environments/environment';
import {exchangeUrls} from './urls';


@Injectable()
export class CurrencyExchangeService {

  constructor(
    private http: HttpClient,
    private store: Store<Root.RootState>
  ) { }
  /*http://data.fixer.io/api/latest?access_key=703777d14e1f57554a14f355b62dd171&symbols=USD,AUD,CAD,PLN,MXN&format=1*/

  fetchRates(data) {
    console.log('data', data);
    const symbol = data.payload.code;
    const params = new HttpParams()
      .set(`access_key`, `${environment.apiKey}`)
      .set(`symbols`, symbol)
      .set(`format`, `1`);
    return this.http.get<ExchangeRate>(exchangeUrls.URL_GET_RATE, {params: params});
  }
}
