import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrencyExchangeComponent } from './currency-exchange.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CurrencyFormatDirective} from '../directives/currency-format/currency-format.directive';
import {OnlyNumbersDirective} from '../directives/onlynumbers/only-numbers.directive';
import * as RootState from '../reducers';
import {Store, StoreModule} from '@ngrx/store';
import * as fromExchange from '../reducers/exchange-state/exchange.actions';
import * as Exchange from '../reducers/exchange-state/exchange.reducer';
import * as fromUi from '../reducers/ui-state/ui.actions';
import * as Ui from '../reducers/ui-state/ui.reducer';
import {By} from '@angular/platform-browser';
import {HttpResponse} from '@angular/common/http';
import {s} from '@angular/core/src/render3';
import {SetExchangeRate} from '../reducers/exchange-state/exchange.actions';
import {ExchangeRate} from '../reducers/exchange-state/model/exchange-rate';

describe('CurrencyExchangeComponent', () => {
  let component: CurrencyExchangeComponent;
  let fixture: ComponentFixture<CurrencyExchangeComponent>;
  let store: Store<RootState.RootState>;
  let request: HttpResponse<ExchangeRate>;
  let response: ExchangeRate;


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CurrencyExchangeComponent,
        CurrencyFormatDirective,
        OnlyNumbersDirective,
      ],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        StoreModule.forRoot(RootState.RootReducers),
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrencyExchangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    store = fixture.debugElement.injector.get(Store);
    request = new HttpResponse({
      body: {
        'success': true,
        'timestamp': 1528989908,
        'base': 'EUR',
        'date': '2018-06-14',
        'rates': {
          'USD': 1.164417,
          'AUD': 1.54693,
          'CAD': 1.519552,
          'PLN': 4.275274,
          'MXN': 24.058489
        }
      },
      status: 200,
    });
    response = request.body;

  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should store to be defined', async(() => {
    expect(store).toBeDefined();
  }));

  it('should the form button be disabled while loading', async(() => {
    const button: HTMLElement = fixture.debugElement.query(By.css('.currency-button')).nativeElement;
    const { initialState } = Ui;
    const action = new fromUi.ToogleLoading();
    const state = Ui.UiReducer(initialState, action);
    fixture.detectChanges();
    expect(button.hasAttribute('disabled')).toBe(true);
  }));


  it('should update the state after fetching from the service', async( () => {
    const { initialState } = Exchange;
    const action = new fromExchange.SetExchangeRate(response);
    const state = Exchange.ExchangeReducer(initialState, action);
    expect(state.rate).toEqual(response);
  }));

  it('should calculate currency exchange', async( () => {
    const { initialState } = Exchange;
    const action = new fromExchange.SetExchangeRate(response);
    let state = Exchange.ExchangeReducer(initialState, action);
    const action_calculate = new fromExchange.CalculateExchange(5);
    state = Exchange.ExchangeReducer(state, action_calculate);
    const currencyResult = 5 / response.rates.USD;
    expect(state.lastExchange).toEqual(currencyResult);
  }));

});
