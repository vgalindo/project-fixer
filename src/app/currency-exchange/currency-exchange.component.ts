import {AfterContentInit, AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {CurrencyPipe} from '@angular/common';
import {FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as fromExchange from '../reducers/exchange-state/exchange.actions';
import * as Exchange from '../reducers/exchange-state/exchange.reducer';
import * as fromUi from '../reducers/ui-state/ui.actions';
import * as Root from '../reducers/index';
import {Store} from '@ngrx/store';
import * as RootState from '../reducers/index';
import {filter, map, tap} from 'rxjs/operators';
import {Subscription} from 'rxjs';
import * as _ from 'lodash';
import {Currency} from '../reducers/exchange-state/model/exchange-rate';


const currencyFromOpt: Currency[] = [
  {value: '$', code: 'USD'}
];
const currencyToOpt: Currency[] = [
  {value: '€', code: 'EUR'}
];

@Component({
  selector: 'app-currency-exchange',
  templateUrl: './currency-exchange.component.html',
  styleUrls: ['./currency-exchange.component.scss'],
  providers: [CurrencyPipe]
})
export class CurrencyExchangeComponent implements OnInit, OnDestroy, AfterViewInit {

  currecyForm: FormGroup;

  loading$;

  exchangeSubscription: Subscription;
  fromCurrencyStatusSubscription: Subscription;
  exchangeResult$ = this.store.select(RootState.getExchangeStateExchange).pipe(
    map(result => this.toCurrency.setValue(this.currencyPipe.transform(result, '€', 'symbol', '1.4-4'))),
  );

  fromOptions = currencyFromOpt;
  toOptions = currencyToOpt;

  constructor(
    private formBuilder: FormBuilder,
    private store: Store<RootState.RootState>,
    private cdr: ChangeDetectorRef,
    private currencyPipe: CurrencyPipe,
  ) {
    this.createCurrencyForm();
  }

  ngOnInit() {
    this.store.dispatch(new fromExchange.FetchExchangeRate(this.fromSelector.value));

    this.loading$ = this.store.select(RootState.getUiStateIsLoading).pipe(
      map( loading => {
        loading ? this.fromCurrency.disable() : this.fromCurrency.enable();
      }),
    );

    this.exchangeSubscription = this.exchangeResult$.subscribe();

    this.fromCurrencyStatusSubscription = this.fromSelector.statusChanges.pipe(
      filter(status => status === 'VALID'),
      tap(() => this.store.dispatch(new fromExchange.SelectCurrency(this.fromSelector.value)))
    ).subscribe();

  }

  createCurrencyForm() {
    this.currecyForm = this.formBuilder.group({
      fromSelector: [this.fromOptions[0], [
        Validators.required,
      ]],
      fromCurrency: ['', [
        Validators.required,
        Validators.pattern('^\\$?\\d{1,3}(\\d{3})*(\\.\\d{1,4})?$')
      ]],
      toSelector: [{value: this.toOptions[0], disabled: true}],
      toCurrency: [{value: '0', disabled: true}, [
      ]],
    });
  }

  get fromCurrency() { return this.currecyForm.get('fromCurrency'); }
  get toCurrency() { return this.currecyForm.get('toCurrency'); }
  get fromSelector() { return this.currecyForm.get('fromSelector'); }
  get toSelector() { return this.currecyForm.get('toSelector'); }

  formSubmit() {
    this.store.dispatch(new fromExchange.CalculateExchange(this.fromCurrency.value));
  }

  ngOnDestroy(): void {
    this.exchangeSubscription.unsubscribe();
    this.fromCurrencyStatusSubscription.unsubscribe();
  }

  ngAfterViewInit(): void {
    this.cdr.detectChanges();
  }
}
