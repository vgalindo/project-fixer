import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CurrencyExchangeComponent} from './currency-exchange/currency-exchange.component';

const routes: Routes = [
  { path: 'currencyex' , component: CurrencyExchangeComponent},

  { path: '', redirectTo: 'currencyex', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
