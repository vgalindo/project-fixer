import {Directive, HostListener} from '@angular/core';
import {isNull} from "util";

@Directive({
  selector: '[appOnlyNumbers]',
})
export class OnlyNumbersDirective {

  constructor() { }

  @HostListener('keydown', ['$event']) onOnlyNumbers(e: KeyboardEvent) {
    const re = new RegExp(`[0-9.]`, 'g');
    if (e.keyCode === 8 || e.keyCode === 13 || !isNull(e.key.match(re))) {
      return;
    }
    e.preventDefault();
  }

}
