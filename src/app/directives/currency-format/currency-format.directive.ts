import {Directive, ElementRef, HostBinding, HostListener, Input} from '@angular/core';
import {CurrencyPipe} from '@angular/common';
import {isNullOrUndefined} from "util";
import {Observable} from 'rxjs';
import {of} from 'rxjs/observable/of';
import {every, filter, map} from 'rxjs/operators';
import {Currency} from '../../reducers/exchange-state/model/exchange-rate';

@Directive({
  selector: '[appCurrencyFormat]'
})
export class CurrencyFormatDirective {

  @Input() currencySimbol: Currency;

  constructor(
    private currencyPipe: CurrencyPipe,
    private el: ElementRef
  ) {}

  @HostListener('change', ['$event']) currencyFormat(e) {
    const element = (this.el.nativeElement as HTMLInputElement).value;
    const condition$: Observable<RegExp> = of(
      new RegExp(`^\\${this.currencySimbol.value}?\\d{1,3}(,?\\d{3})*(\\.\\d{1,4})$`)
    );

    const checkingString$ = condition$.pipe(
      every(urlMatch => !isNullOrUndefined(element.match(element))),
      filter(value => value === true),
      map((value) => {
        const currency = value ? element.replace(new RegExp(`\\[${this.currencySimbol.value},]+`, 'g'), '') : 0;
        (this.el.nativeElement as HTMLInputElement).value = this.currencyPipe.transform(+currency, this.currencySimbol.value, 'symbol', '1.4-4');
      }),
    ).subscribe();

  }

}
