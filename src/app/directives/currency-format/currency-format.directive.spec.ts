import { CurrencyFormatDirective } from './currency-format.directive';
import {ElementRef} from '@angular/core';
import {CurrencyPipe} from '@angular/common';

const currencyPipe: CurrencyPipe = new CurrencyPipe('');
const el: ElementRef = new ElementRef('');

describe('CurrencyFormatDirective', () => {
  it('should create an instance', () => {
    const directive = new CurrencyFormatDirective(
      currencyPipe, el
    );
    expect(directive).toBeTruthy();
  });
});
