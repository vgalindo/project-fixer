import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {environment} from '../environments/environment';
import { StoreModule } from '@ngrx/store';
import { RootReducers, metaReducers } from './reducers';
import { EffectsModule } from '@ngrx/effects';
import { AppEffects } from './app.effects';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { CurrencyExchangeComponent } from './currency-exchange/currency-exchange.component';
import {OnlyNumbersDirective} from './directives/onlynumbers/only-numbers.directive';
import {CurrencyFormatDirective} from './directives/currency-format/currency-format.directive';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CurrencyExchangeService} from './currency-exchange/services/currency-exchange.service';
import {ExchangeEffects} from './reducers/exchange-state/exchange.effects';
import {UiEffects} from './reducers/ui-state/ui.effects';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    CurrencyExchangeComponent,
    OnlyNumbersDirective,
    CurrencyFormatDirective,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    StoreModule.forRoot(RootReducers, { metaReducers }),
    EffectsModule.forRoot([
      AppEffects,
      ExchangeEffects,
      UiEffects,
    ]),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
  ],
  exports: [
    OnlyNumbersDirective,
    CurrencyFormatDirective,
  ],
  providers: [
    CurrencyExchangeService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
